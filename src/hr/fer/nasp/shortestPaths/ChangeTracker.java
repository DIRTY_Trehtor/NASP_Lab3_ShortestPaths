package hr.fer.nasp.shortestPaths;

import java.util.HashSet;

/**
 * Helps take record of last iteration changes. This records save time doing
 * next iteration because only previous iteration changed vertices, and earlier
 * this iteration changed, have to be checked.
 * 
 * @author Janko
 *
 */
class ChangeTracker {

	private HashSet<String> recentChanges;
	private HashSet<String> thisIterationChanges;

	/**
	 * Constructor taking source vertex and including it into zero iteration change
	 * so that first iteration has one starting vertex to work with.
	 * 
	 * @param sourceVertex
	 *            Name of the source vertex.
	 */
	ChangeTracker(String sourceVertex) {
		recentChanges = new HashSet<>();
		recentChanges.add(sourceVertex);
		thisIterationChanges = new HashSet<>();
	}

	/**
	 * Checks if vertex was not changed last iteration or earlier this iteration.
	 * 
	 * @param vertexName Vertex name.
	 * @return True if it was not changed recently.
	 */
	boolean isNotChanged(String vertexName) {
		return !recentChanges.contains(vertexName);
	}

	/**
	 * Mark (record) changed vertex.
	 * @param vertexName Name of vertex being changed.
	 */
	void markChangedVertex(String vertexName) {
		this.recentChanges.add(vertexName);
		this.thisIterationChanges.add(vertexName);
	}

	/**
	 * At the end of iteration new changes have to be prepared for next iteration to be worked on.
	 */
	void endOfIteration() {
		this.recentChanges = this.thisIterationChanges;
		this.thisIterationChanges = new HashSet<>();
	}
	
	@Override 
	public String toString() {
		String text = "recent:\t\t" + this.recentChanges.toString() + "\nthisIter:\t" + this.thisIterationChanges.toString();
		return text;
	}

}
