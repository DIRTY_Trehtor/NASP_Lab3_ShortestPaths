package hr.fer.nasp.shortestPaths;

import java.util.ArrayList;
import java.util.List;

/**
 * Vertex in a graph.
 * 
 * @author Janko
 *
 */
class Vertex {

	/**
	 * Vertex name.
	 */
	private final String name;
	/**
	 * Weight label for shortest path algorithm.
	 */
	private int weightLabel;
	private boolean isInRange;
	/**
	 * Parent vertex. Used for back-propagation to build shortest path.
	 */
	private Vertex parent;
	/**
	 * List of output edges of this vertex.
	 */
	private List<OutputEdge> outputEdges;

	/**
	 * Creates object by it's name only. Initializes empty array of output edges.
	 * 
	 * @param name
	 *            Name of a vertex.
	 */
	Vertex(String name) {
		this.name = name;
		this.outputEdges = new ArrayList<>();
		
		this.weightLabel = 0;
		this.isInRange = false;//label infinity
		
		this.parent = null;
	}

	String getName() {
		return this.name;
	}
	void setWeightLabel(Integer value) {
		this.weightLabel = value;
	}

	int getWeightLabel() {
		return this.weightLabel;
	}

	void setParent(Vertex parent) {
		this.parent = parent;
	}

	Vertex getParent() {
		return this.parent;
	}

	void addOutputEdge(Vertex neighborVertex, int weight) {
		OutputEdge newOutEdge = new OutputEdge(neighborVertex, weight);
		this.outputEdges.add(newOutEdge);
	}

	List<OutputEdge> getOutputEdges() {
		return this.outputEdges;
	}
	void inRange() {
		this.isInRange = true;
	}
	boolean IsInRange() {
		return this.isInRange;
	}
	
	@Override
	public String toString() {
		
		String toReturn = "";
		toReturn += "Name:" + this.name + ", InRange:" + this.isInRange + ", Label:" + this.weightLabel + "";
		toReturn += ", Edges: ";
		for(OutputEdge oe : this.outputEdges) {
			toReturn += oe.getNeighborVertex().getName() + "(" + oe.getEdgeWeight() + ") ";
		}
		
		return toReturn;
	}

}
