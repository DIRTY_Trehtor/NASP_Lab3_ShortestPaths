package hr.fer.nasp.testing;
import java.io.FileNotFoundException;

import javax.naming.directory.InvalidAttributesException;

import hr.fer.nasp.shortestPaths.*;

public class Main {

	public static void main(String[] args) {
		
		String configurationFile1 = "./gridConfigurations/config2";
		
		try {
			
			ShortestPathCalculator calculator = new ShortestPathCalculator(configurationFile1);
			
			ShortestPath sp = calculator.calculateShortestPath("a", "k");
			
			sp.printShortestPath();
			
		} catch (FileNotFoundException e) {
			System.err.println("Wrong file name or path to it.");
		} catch (InvalidAttributesException e) {
			System.err.println("Vertices, given to calculate shortest path between, do not exist.");
		} catch (InfinityCycleException e) {
			System.err.println("There is an infinite cycle present in graph.");
		}
		

	}
 
}
