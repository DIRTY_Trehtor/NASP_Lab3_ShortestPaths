package hr.fer.nasp.shortestPaths;

/**
 * Represents an edge from one vertex towards it's neighbor vertex. Second
 * vertex is stored as an attribute of this class, but first one is represented by vertex
 * holding one specific instance of OutputEdge object.
 * 
 * @author Janko
 *
 */
class OutputEdge {

	/**
	 * Vertex this edge is pointing to.
	 */
	private final Vertex neighborVertex;
	/**
	 * Edge's weight.
	 */
	private final int weight;

	/**
	 * Constructor sets all two attributes.
	 * @param neighborVertex Vertex edge is pointing to.
	 * @param weight Edge's weight.
	 */
	OutputEdge(Vertex neighborVertex, int weight) {
		this.neighborVertex = neighborVertex;
		this.weight = weight;
	}
	
	/** 
	 * @return Neighbor vertex (vertex edge is pointing to).
	 */
	Vertex getNeighborVertex() {
		return this.neighborVertex;
	}
	
	/** 
	 * @return Edge's weight.
	 */
	int getEdgeWeight() {
		return this.weight;
	}
	
	@Override
	public String toString() {
		return this.neighborVertex.getName() + " (" + this.weight + ")";
	}

}
