package hr.fer.nasp.shortestPaths;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.naming.directory.InvalidAttributesException;

/**
 * Loads grid from it's configuration file. After loading grid, it can calculate
 * shortest path between any two existing vertices.
 * 
 * @author Janko
 *
 */
public class ShortestPathCalculator {

	/**
	 * Path to grid's configuration file.
	 */
	private String path;
	/**
	 * Name of the configuration. Read from configuration.
	 */
	private String configurationName;
	/**
	 * Map of vertices objects present in a grid.
	 */
	private HashMap<String, Vertex> vertices;
	/**
	 * Number of vertices in a grid.
	 */
	private int verticesCount;

	/**
	 * Loads and initializes whole grid by given configuration document path.
	 * 
	 * @param path
	 *            Path to the grid configuration file we are working with.
	 * @throws FileNotFoundException
	 *             If file to given path can not be open.
	 */
	public ShortestPathCalculator(String path) throws FileNotFoundException {

		this.path = path;
		this.vertices = new HashMap<>();
		this.verticesCount = 0;

		loadConfigurationAndInitializeNetwork();
	}

	/**
	 * Opens buffered reader to read from configuration document. If successfully
	 * opened, reads line by line and parses values to create grid model.
	 * 
	 * @throws FileNotFoundException
	 *             If file is not present and buffered reader can not be opened.
	 */
	private void loadConfigurationAndInitializeNetwork() throws FileNotFoundException {

		final String innerSplitter = ",";
		final String outerSplitter = "\\|";

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(this.path)))) {

			this.configurationName = reader.readLine();

			// vertices names
			String verticesNames = reader.readLine();
			String[] verticesSplitted = verticesNames.split(innerSplitter);
			for (String name : verticesSplitted) {
				this.vertices.put(name, new Vertex(name));
				this.verticesCount++;
			}

			// edges
			String line = "";
			while ((line = reader.readLine()) != null) {

				String[] edges = line.split(outerSplitter);
				for (String edge : edges) {

					String[] splitted = edge.split(innerSplitter);
					String vertexName = splitted[0];
					String neighborName = splitted[1];
					String weight = splitted[2];

					Vertex vertex = this.vertices.get(vertexName);
					Vertex neighborVertex = this.vertices.get(neighborName);
					int weightValue = Integer.parseInt(weight);

					vertex.addOutputEdge(neighborVertex, weightValue);
				}
			}

		} catch (IOException e) {
			System.err.println("IO Exception while working with buffered reader.");
		}
	}

	/**
	 * Calculates shortest path, and it's weight, between two vertices from a grid
	 * model initialized earlier in the constructor.
	 * 
	 * @param sourceVertex
	 *            Name of source vertex.
	 * @param destinatonVertex
	 *            Name of destination vertex.
	 * @return ShortestPath object.
	 * @throws InfinityCycleException
	 *             If infinity cycle is found. Algorithm can not be
	 *             finished.
	 * @throws InvalidAttributesException
	 *             If given vertices do not exist in grid model.
	 */
	public ShortestPath calculateShortestPath(String sourceVertex, String destinatonVertex)
			throws InfinityCycleException, InvalidAttributesException {

		if (!verticesExist(sourceVertex, destinatonVertex))
			throw new InvalidAttributesException();

		prepareLabelsForStart(sourceVertex);

		ChangeTracker changeTracker = new ChangeTracker(sourceVertex);
		int iterationsCount = this.verticesCount - 1;
		int i = 1;
		for (; i <= iterationsCount; i++) {
			boolean parametersChanged = doOneIteration(changeTracker);
			if (!parametersChanged)
				break;
		}

		if (i >= iterationsCount)// if it ended before iterations limit, it does not have infinite cycle.
			if (existsInfiniteCycles(changeTracker))
				throw new InfinityCycleException();

		// create ShortestPath object and return
		return createResponseFromCalculatedData(sourceVertex, destinatonVertex);
	}

	/**
	 * Extracts overall path weight from data and creates string representation of
	 * shortest path trajectory from source vertex to destination vertex.
	 * 
	 * @param sourceVertex
	 *            Name of source vertex.
	 * @param destinationVertex
	 *            name of destination vertex.
	 * @return Object type ShortestPath.
	 */
	private ShortestPath createResponseFromCalculatedData(String sourceVertex, String destinationVertex) {

		Vertex destVertex = this.vertices.get(destinationVertex);
		int shortestPathWeight = destVertex.getWeightLabel();

		String shortestPath = "";
		Vertex v = destVertex;
		while (v != null) {
			shortestPath = " => " + v.getName() + shortestPath;
			v = v.getParent();
		}

		return new ShortestPath(shortestPath, shortestPathWeight, sourceVertex, destinationVertex);
	}

	/**
	 * Source vertex's attribute "inRange" is set to true, and other vertices
	 * "inRange" attributes are by default on false (meaning infinity).
	 * 
	 * @param sourceVertex Name of a source vertex.
	 */
	private void prepareLabelsForStart(String sourceVertex) {
		
		this.vertices.get(sourceVertex).inRange();
	}

	/**
	 * Does one iteration of shortest path algorithm. Iterates through reached
	 * vertices (ones whose labels are not infinity). Checks if any of the output
	 * edges maybe shortens overall path weight to neighbor vertex. If true, sets
	 * smaller weight.
	 * 
	 * @param changeTracker
	 *            Object that takes record of changes being made to neighbor weight
	 *            labels. There is significant speed up if in the next iteration
	 *            only changed vertices are processed.
	 * @return True if at least one change in weight labels had been made, false if
	 *         there were no changes.
	 */
	private boolean doOneIteration(ChangeTracker changeTracker) {

		boolean change = false;

		List<String> keys = new ArrayList<>(this.vertices.keySet());
		for (String key : keys) {

			Vertex vertex = this.vertices.get(key);
			if (!vertex.IsInRange() || changeTracker.isNotChanged(key))// infinity
				continue;

			for (OutputEdge edge : vertex.getOutputEdges()) {

				int weightLabel = vertex.getWeightLabel();
				int edgeWeight = edge.getEdgeWeight();

				int neighborsNewWeightLabel = weightLabel + edgeWeight;
				int neighborsOldWeightLabel = edge.getNeighborVertex().getWeightLabel();

				Vertex neighborVertex = edge.getNeighborVertex();
				if (neighborsNewWeightLabel < neighborsOldWeightLabel || !neighborVertex.IsInRange()) {

					neighborVertex.setWeightLabel(neighborsNewWeightLabel);
					neighborVertex.setParent(vertex);
					neighborVertex.inRange();

					changeTracker.markChangedVertex(neighborVertex.getName());
					change = true;
				}
			}
		}

		changeTracker.endOfIteration();
		return change;
	}

	/**
	 * Method checks if there is infinite loop in a graph. By the theory of shortest
	 * path algorithm, algorithm must be solved in a fixed amount of steps. If after
	 * even one more step there are changes, there is an infinite cycle present in a
	 * graph.
	 * 
	 * @param changeTracker
	 *            Object that takes record of changes being made to neighbor weight
	 *            labels. There is significant speed up if in the next iteration
	 *            only changed vertices are processed.
	 * 
	 * @return True if there is an infinite cycle, false if infinite cycles arent
	 *         present.
	 */
	private boolean existsInfiniteCycles(ChangeTracker changeTracker) {
		return doOneIteration(changeTracker);

	}

	/**
	 * Checks if source and destination vertices are present in a graph model.
	 * Important check before starting shortest path algorithm.
	 * 
	 * @param sourceVertex
	 *            Source vertex name.
	 * @param destinatonVertex
	 *            Destination vertex name.
	 * @return True if both vertices exist, false if not.
	 */
	private boolean verticesExist(String sourceVertex, String destinatonVertex) {

		boolean first = this.vertices.containsKey(sourceVertex);
		boolean second = this.vertices.containsKey(destinatonVertex);

		return (first && second);
	}

	/**
	 * @return Name of the configuration this calculator is working on.
	 */
	public String getConfigurationName() {
		return this.configurationName;
	}

}
