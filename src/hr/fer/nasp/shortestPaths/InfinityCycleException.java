package hr.fer.nasp.shortestPaths;

/**
 * Exception to catch and record case if there is an infinity cycle in a graph.
 * @author Janko
 *
 */
public class InfinityCycleException extends Exception {

	/**
	 * Generated serial UID.
	 */
	private static final long serialVersionUID = -2296826096085470677L;
	
	public InfinityCycleException() {
		super();
	}
	public InfinityCycleException(String message) {
		super(message);
	}
	public InfinityCycleException(String message, Throwable cause) {
		super(message, cause);
	}
	public InfinityCycleException(Throwable cause) {
		super(cause);
	}

}
