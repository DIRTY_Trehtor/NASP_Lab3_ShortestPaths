package hr.fer.nasp.shortestPaths;

/**
 * Represents shortest path algorithm final calculation. Object of this class
 * can not be instantiated outside the package "hr.fer.nasp.shortestPaths". It
 * is given to user as result of ShortestPathCalculator calculation. Some of
 * it's implementations are methods for: printing shortest path trajectory and
 * it's weight, getting source and destination vertex, and other similar to the
 * topic. All of it's attributes are final, so they can not be changed, which is
 * logical due it's nature as calculation result.
 * 
 * @author Janko
 *
 */
public class ShortestPath {

	/**
	 * String representation of shortest path. Format "node1 -> node2 -> ...
	 * ->nodeN".
	 * 
	 */
	private final String shortestPath;
	/**
	 * Weight of shortest path.
	 */
	private final int shortestPathWeight;
	/**
	 * Name of the source vertex in process of finding shortest path.
	 */
	private final String sourceVertex;
	/**
	 * Name of the destination vertex in process of finding shortest path.
	 */
	private final String destinationVertex;

	/**
	 * Sets all the attributes of this class, which are all final.
	 * 
	 * @param shortestPath
	 *            String representation of shortest path. Format "node1 -> node2 ->
	 *            ... ->nodeN".
	 * @param shortestPathWeight
	 *            Weight of shortest path.
	 * @param sourceVertex
	 *            Name of the source vertex in process of finding shortest path.
	 * @param destinationVertex
	 *            Name of the destination vertex in process of finding shortest
	 *            path.
	 */
	ShortestPath(String shortestPath, int shortestPathWeight, String sourceVertex, String destinationVertex) {
		this.shortestPath = shortestPath;
		this.shortestPathWeight = shortestPathWeight;
		this.sourceVertex = sourceVertex;
		this.destinationVertex = destinationVertex;
	}

	/**
	 * Prints weight of shortest path and array of vertexes that are part of
	 * shortest path (from source to destination vertex).
	 */
	public void printShortestPath() {
		String text = "";
		text += "Shortest path from " + this.sourceVertex + " to " + this.destinationVertex + ":\n";
		text += "\t" + "weight: " + this.shortestPathWeight + "\n";
		text += "\t" + this.shortestPath + "\n";
		System.out.print(text);
	}

	/**
	 * Shortest path from source vertex to destination vertex.
	 * 
	 * @return Shortest path represented as string.
	 */
	public String getShortestPath() {
		return this.shortestPath;
	}

	/**
	 * Shortest path weight.
	 * 
	 * @return Shortest path weight represented as integer number.
	 */
	public int getShortestPathWeight() {
		return this.shortestPathWeight;
	}

	/**
	 * Source vertex.
	 * 
	 * @return Source vertex as string;
	 */
	public String getSourceVertex() {
		return this.sourceVertex;
	}

	/**
	 * Destination vertex.
	 * 
	 * @return Destination vertex as string;
	 */
	public String getDestinationVertex() {
		return this.destinationVertex;
	}
}
